# BLACKJACK

#### ABOUT
This is Blackjack game for one player, who plays against the dealer (computer), 6 standard 52-card decks are shuffled together. Cards value are:
 - 2-10 = face value
 - J,Q,K = 10
 - A = 11 or 1 - depends what is better for player/dealer

#### GOAL
The goal of a game is simply beat the dealer in one of the following ways:
- Get 21 points on the player's first two cards (called a Blackjack) without a dealer Blackjack
- Reach a final score higher than the dealer without exceeding 21
- Let the dealer draw additional cards until their hand exceeds 21
##### Note: Blackjack is paid 3:2, other winning round is paid 1:1

#### GAMEPLAY
- firstly, you need to provide your buy-in
 ![picture](src/main/resources/other/buy_in.PNG)

- before dealer deals cards, player places a bet
![picture](src/main/resources/other/place_your_bet.PNG)

- both the player and the dealer get 2 cards. Player cards are face-up, one dealer's card is face-down.
![picture](src/main/resources/other/deal.PNG)

- if you’re dealt a ten-value card and an Ace as your first two cards that means you got a Blackjack! Those get paid 3:2 as long as the dealer doesn't also have a Blackjack. If the dealer also has 
    a Blackjack, you wouldn’t win anything but you also wouldn’t lose your original bet.
- if you don't have Blackjack and dealer doesn't have Blackjack too- there are 3 ways you can play your hand:

    *  Stand - if your two cards are acceptable, you can stand and it is the dealer turn

    * Hit -  if you would like more cards to improve your hand total, the dealer will deal you more cards,
one at a time
    * Double - If you have a hand total that is advantageous to you but you need to take an 
additional card you can double your initial bet and the dealer will deal you only 1 additional card.
    
 When player finishes his moves and didn't loose yet, it is dealer's turn.  
 
 If the dealer has a hand total of 17 or higher, he will automatically stand. 
 If the dealer has a hand total of 16 or lower, they will take additional hit-cards. 
 However, there are 2 cases when it comes to 17 - hard 17 and soft 17.
 Hard situation is when the dealer has 17 without Ace. Then he will stand. 
 In case of soft 17 (17 including Ace with value of 11), 
 the dealer hit the card. In case when the dealer exceed 21, Ace will have value of 1. The dealer does not have any choice with how they play their hand like the player does.
  
 When the round is finished, you can play next one (until you have stack available) or exit a game. 
 
 ![picture](src/main/resources/other/end_of_round.PNG)