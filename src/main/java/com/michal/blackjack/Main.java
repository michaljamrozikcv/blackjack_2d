package com.michal.blackjack;

import com.michal.blackjack.controller.BlackjackController;
import com.michal.blackjack.controller.BlackjackControllerImpl;
import com.michal.blackjack.model.DealingShoeImpl;
import com.michal.blackjack.model.Gamer;
import com.michal.blackjack.model.Player;
import com.michal.blackjack.model.Round;

public class Main {
    public static void main(String[] args) {
        Gamer player = new Player();
        Gamer dealer = new Gamer();
        DealingShoeImpl dealingShoeImpl = new DealingShoeImpl();
        Round round = new Round();
        BlackjackController controller = new BlackjackControllerImpl(player, dealer, round, dealingShoeImpl);
        controller.createGame();
    }
}


