package com.michal.blackjack.controller;

public interface BlackjackController {
    void createGame();

    void startGame();

    void updateStack(double amount);

    void updateBet(int amount);

    void enableBetPanel();

    void nextRound();

    void deal();

    void hit();

    void stand();

    void doubleBet();

    void exit();
}
