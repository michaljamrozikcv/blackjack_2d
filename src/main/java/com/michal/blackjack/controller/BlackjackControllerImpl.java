package com.michal.blackjack.controller;

import com.michal.blackjack.model.*;
import com.michal.blackjack.view.BlackjackView;
import com.michal.blackjack.view.BlackjackViewImpl;
import com.michal.blackjack.view.components.ChipButton;
import com.michal.blackjack.view.components.Formatter;

import java.util.List;

public class BlackjackControllerImpl implements BlackjackController {
    private final Gambler player;
    private final Gambler dealer;
    private final Round round;
    private final DealingShoe dealingShoe;
    private BlackjackView view;

    public BlackjackControllerImpl(Gambler player, Gambler dealer, Round round, DealingShoe dealingShoe) {
        this.player = player;
        this.dealer = dealer;
        this.round = round;
        this.dealingShoe = dealingShoe;
    }

    @Override
    public void createGame() {
        view = new BlackjackViewImpl(this, player, dealer, round);
        view.createView();
        dealingShoe.shuffle();
    }

    @Override
    public void startGame() {
        int initialStack = 0;
        try {
            initialStack = Integer.parseInt(view.getInitialStack());
        } catch (NumberFormatException e) {
            view.setMessage("Incorrect initial stack. Please try again");
            return;
        }
        if (initialStack > 0) {
            updateStack(initialStack);
            view.enableOptionsPanel();
            enableBetPanel();
            view.disableStartButton();
            view.enableExitButton();
            view.enableCardsPanels();
            view.setMessage("Place your bet");
            view.disableInitialStackTExtField();
        } else {
            view.setMessage("Initial stack has to be positive amount. Please re-enter amount");
        }
    }

    @Override
    public void updateStack(double amount) {
        Player p = (Player) player;
        p.updateStack(amount);
    }

    @Override
    public void updateBet(int amount) {
        if (round.getBet() == 0) {
            view.enableDealButton();
        }
        round.updateBet(amount);
        updateStack(-amount);
        disableChips(getPlayerStack());
    }

    @Override
    public void enableBetPanel() {
        view.enableBetPanel();
        disableChips(getPlayerStack());
    }

    @Override
    public void nextRound() {
        view.disableMessagePanel();
        player.resetCards();
        dealer.resetCards();
        view.clearCards();
        if (getPlayerStack() >= 1) {
            view.setMessage("Place your bet");
        } else {
            view.setMessage("Your stack is too low. Please press EXIT");
        }
        view.disableNextRoundButton();
        enableChips(getPlayerStack());
    }

    @Override
    public void deal() {
        view.disableMessagePanel();
        dealTwoCards();
        view.disableAllChips();
        if (dealer.getCardsValue() == 21 || player.getCardsValue() == 21) {
            //blackjack cases
            if (dealer.getCardsValue() == 21 && player.getCardsValue() == 21) {
                //draw - dealer and player have Blackjack
                view.showAllDealerCards();
                view.setMessage("Draw. Dealer and player have BLACKJACK");
                updateStack(round.getBet());
            } else if (dealer.getCardsValue() == 21) {
                //dealer has Blackjack
                view.showAllDealerCards();
                view.setMessage("Dealer has BLACKJACK. You lose");
            } else {
                //player has Blackjack
                view.setMessage("BLACKJACK! You win");
                updateStack(2.5 * round.getBet());
            }
            prepareSettingsForNextRound();
        } else {
            //case no blackjack
            view.disableDealButton();
            view.enableHitButton();
            view.enableStandButton();
            if (getPlayerStack() >= round.getBet()) {
                view.enableDoubleButton();
            } else {
                view.disableDoubleButton();
            }
        }
    }

    @Override
    public void hit() {
        player.hit(dealingShoe.takeCard());
        view.disableDoubleButton();
        view.showPlayerNextCard();
        if (player.getCardsValue() >= 21) {
            if (player.getCardsValue() == 21) {
                dealMoreCardsToDealer();
                if (dealer.getCardsValue() == 21) {
                    //draw - dealer has 21 too
                    view.setMessage("Draw");
                    updateStack(round.getBet());
                } else {
                    //player win
                    view.setMessage("You win");
                    updateStack(2 * round.getBet());
                }
                view.showAllDealerCards();
            } else if (player.getCardsValue() > 21) {
                view.setMessage("You lose. Value higher than 21");
            }
            prepareSettingsForNextRound();
        }
    }

    @Override
    public void stand() {
        dealMoreCardsToDealer();
        if (dealer.getCardsValue() > 21) {
            view.setMessage("You win. Dealer value higher than 21");
            updateStack(2 * round.getBet());
        } else if (dealer.getCardsValue() > player.getCardsValue()) {
            view.setMessage("You lose");
        } else if (dealer.getCardsValue() < player.getCardsValue()) {
            view.setMessage("You win");
            updateStack(2 * round.getBet());
        } else {
            view.setMessage("Draw");
            updateStack(round.getBet());
        }
        view.showAllDealerCards();
        prepareSettingsForNextRound();
    }

    @Override
    public void doubleBet() {
        view.disableAllChips();
        view.disableOptionButtons();
        updateBet(round.getBet());
        player.hit(dealingShoe.takeCard());
        view.showPlayerNextCard();
        if (player.getCardsValue() > 21) {
            view.setMessage("You lose. Value higher than 21");
            prepareSettingsForNextRound();
        } else {
            dealMoreCardsToDealer();
            stand();
        }
    }

    @Override
    public void exit() {
        int initialStack = Integer.parseInt(view.getInitialStack());
        double difference = getPlayerStack() - initialStack;
        if (difference > 0) {
            view.setMessage("GAME OVER. You won  " + Formatter.displayAmount(difference));
        } else if (difference < 0) {
            difference = -difference;
            view.setMessage("GAME OVER. You lost  " + Formatter.displayAmount(difference));
        } else {
            view.setMessage("GAME OVER. Your current stack is equal to initial stack");
        }
        view.disableNextRoundButton();
        view.disableExitButton();
        view.disableAllChips();
    }

    private void enableChips(double remainingStack) {
        BlackjackViewImpl v = (BlackjackViewImpl) view;
        List<ChipButton> chipButtons = v.getBetPanel().getListOfChips();
        chipButtons.stream()
                .filter(chipButton -> chipButton.getValue() <= remainingStack)
                .forEach(chipButton -> chipButton.setEnabled(true));
    }

    private void disableChips(double remainingStack) {
        BlackjackViewImpl v = (BlackjackViewImpl) view;
        List<ChipButton> chipButtons = v.getBetPanel().getListOfChips();
        chipButtons.stream()
                .filter(chipButton -> chipButton.getValue() > remainingStack)
                .forEach(chipButton -> chipButton.setEnabled(false));
    }

    private double getPlayerStack() {
        Player p = (Player) player;
        return p.getStack();
    }

    private void dealTwoCards() {
        player.hit(dealingShoe.takeCard());
        dealer.hit(dealingShoe.takeCard());
        player.hit(dealingShoe.takeCard());
        dealer.hit(dealingShoe.takeCard());
        view.showDealerTwoCardsSecondHidden();
        view.showPlayerFirstTwoCards();
    }

    private void dealMoreCardsToDealer() {
        while (dealer.getCardsValue() <= 16 || (dealer.getCardsValue() == 17 && dealer.getQuantityOfAces() > 0)) {
            dealer.hit(dealingShoe.takeCard());
        }
    }

    private void prepareSettingsForNextRound() {
        view.disableOptionButtons();
        view.enableNextRoundButton();
        round.clearBet();
        view.enableExitButton();
    }
}
