package com.michal.blackjack.model;

public interface DealingShoe {
    PlayingCard takeCard();

    void shuffle();
}
