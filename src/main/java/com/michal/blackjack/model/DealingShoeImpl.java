package com.michal.blackjack.model;

import lombok.Getter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Getter
public class DealingShoeImpl implements DealingShoe {
    private final List<PlayingCard> listOfCards;
    private final int quantityOfDecks = 6;

    public DealingShoeImpl() {
        listOfCards = new ArrayList<>();
        for (int i = 1; i <= quantityOfDecks; i++) {
            listOfCards.addAll(new Deck().getListOfCards());
        }
    }

    @Override
    public PlayingCard takeCard() {
        if (listOfCards.size() > 0) {
            return listOfCards.remove(0);
        }
        return null;
    }

    @Override
    public void shuffle() {
        Collections.shuffle(listOfCards);
    }

    @Override
    public String toString() {
        return listOfCards.toString();
    }
}
