package com.michal.blackjack.model;

import lombok.Getter;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Getter
public class Deck {
    private final List<PlayingCard> listOfCards = Stream.of(Rank.values())
            .flatMap(rank -> Stream.of(Suit.values())
                    .map(suit -> new PlayingCard(rank, suit)))
            .collect(Collectors.toList());

    @Override
    public String toString() {
        return listOfCards.toString();
    }
}
