package com.michal.blackjack.model;

import java.util.List;

public interface Gambler extends Observable {
    void hit(PlayingCard card);

    void resetCards();

    int getCardsValue();

    int getQuantityOfAces();

    List<PlayingCard> getListOfCards();
}
