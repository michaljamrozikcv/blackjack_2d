package com.michal.blackjack.model;

import com.michal.blackjack.view.Observer;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@Getter
public class Gamer implements Gambler {
    private final List<PlayingCard> listOfCards;
    private final List<Observer> observers;
    private int cardsValue;
    private int quantityOfAces;//Aces with value of 11

    public Gamer() {
        listOfCards = new ArrayList<>();
        observers = new ArrayList<>();
    }

    public void hit(PlayingCard card) {
        listOfCards.add(card);
        if (card.getValue() == 11) {
            quantityOfAces++;
        }
        cardsValue += card.getValue();
        while (quantityOfAces > 0 && cardsValue > 21) {
            cardsValue -= 10;
            quantityOfAces--;
        }
    }

    public void resetCards() {
        listOfCards.clear();
        cardsValue = 0;
        quantityOfAces = 0;
    }

    @Override
    public void registerObserver(Observer o) {
        observers.add(o);
    }

    @Override
    public void removeObserver(Observer o) {
        observers.remove(o);
    }

    @Override
    public void notifyObservers() {
        observers.forEach(observer -> observer.updateStack());
    }
}
