package com.michal.blackjack.model;

import com.michal.blackjack.view.Observer;

public interface Observable {
    void registerObserver(Observer o);

    void removeObserver(Observer o);

    void notifyObservers();
}
