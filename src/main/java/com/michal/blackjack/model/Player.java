package com.michal.blackjack.model;

import lombok.Getter;

@Getter
public class Player extends Gamer {
    private double stack;

    public void updateStack(double amount) {
        stack += amount;
        notifyObservers();
    }
}
