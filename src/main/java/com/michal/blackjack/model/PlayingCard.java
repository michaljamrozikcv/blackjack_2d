package com.michal.blackjack.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class PlayingCard {
    private final Rank rank;
    private final Suit suit;

    public int getValue() {
        return rank.getValue();
    }

    @Override
    public String toString() {
        return rank.getRank() + suit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PlayingCard that = (PlayingCard) o;
        if (rank != that.rank) return false;
        return suit == that.suit;
    }

    @Override
    public int hashCode() {
        int result = rank != null ? rank.hashCode() : 0;
        result = 31 * result + (suit != null ? suit.hashCode() : 0);
        return result;
    }
}


