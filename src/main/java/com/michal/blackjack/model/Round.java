package com.michal.blackjack.model;

import com.michal.blackjack.view.Observer;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@Getter
public class Round implements Observable {
    private final List<Observer> observers;
    private int bet;

    public Round() {
        observers = new ArrayList<>();
    }

    public void updateBet(int amount) {
        bet += amount;
        notifyObservers();
    }

    public void clearBet() {
        bet = 0;
        notifyObservers();
    }

    @Override
    public void registerObserver(Observer o) {
        observers.add(o);
    }

    @Override
    public void removeObserver(Observer o) {
        observers.remove(o);
    }

    @Override
    public void notifyObservers() {
        observers.forEach(Observer::updateBet);
    }
}
