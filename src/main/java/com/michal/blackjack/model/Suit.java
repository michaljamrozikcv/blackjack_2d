package com.michal.blackjack.model;

public enum Suit {
    CLUB,
    DIAMOND,
    HEART,
    SPADE
}
