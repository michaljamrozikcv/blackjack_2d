package com.michal.blackjack.view;

public interface BlackjackView {
    void createView();

    void enableOptionsPanel();

    void enableBetPanel();

    void enableCardsPanels();

    void enableMessagePanel();

    void disableMessagePanel();

    void enableNextRoundButton();

    void disableNextRoundButton();

    void enableDealButton();

    void disableDealButton();

    void enableHitButton();

    void enableStandButton();

    void enableDoubleButton();

    void disableDoubleButton();

    void disableStartButton();

    void disableAllChips();

    void disableOptionButtons();

    void enableExitButton();

    void disableExitButton();

    void showPlayerFirstTwoCards();

    void showPlayerNextCard();

    void showDealerTwoCardsSecondHidden();

    void showAllDealerCards();

    void clearCards();

    void setMessage(String message);

    String getInitialStack();

    void disableInitialStackTExtField();
}

