package com.michal.blackjack.view;

import com.michal.blackjack.controller.BlackjackController;
import com.michal.blackjack.model.*;
import com.michal.blackjack.view.components.*;
import lombok.Getter;
import lombok.Setter;

import javax.swing.*;
import java.awt.*;

@Getter
@Setter
public class BlackjackViewImpl implements Observer, BlackjackView {
    private final Round round;
    private final Gambler player;
    private final Gambler dealer;
    private final BlackjackController controller;
    private JFrame mainFrame;
    private JPanel backgroundPanel;
    private HandCardsPanel playerCardsPanel;
    private HandCardsPanel dealerCardsPanel;
    private StackPanel stackPanel;
    private BetPanel betPanel;
    private OptionsPanel optionsPanel;
    private MessagePanel messagePanel;
    private JLabel playerValueLabel;
    private JLabel dealerValueLabel;

    public BlackjackViewImpl(BlackjackController controller, Gambler player, Gambler dealer, Round round) {
        this.controller = controller;
        this.player = player;
        this.dealer = dealer;
        this.round = round;
        player.registerObserver(this);
        round.registerObserver(this);
    }

    @Override
    public void createView() {
        //frame and background
        mainFrame = new MainFrame();
        backgroundPanel = new BackgroundPanel("/other/felt.jpeg");
        mainFrame.setContentPane(backgroundPanel);

        //player hand
        playerCardsPanel = new HandCardsPanel("PLAYER", 600, 430, player.getListOfCards());
        playerValueLabel = new CardValueLabel(String.valueOf(player.getCardsValue()));
        playerCardsPanel.add(playerValueLabel);
        backgroundPanel.add(playerCardsPanel);

        //dealer hand
        dealerCardsPanel = new HandCardsPanel("DEALER", 600, 50, dealer.getListOfCards());
        dealerValueLabel = new CardValueLabel(String.valueOf(dealer.getCardsValue()));
        dealerCardsPanel.add(dealerValueLabel);
        backgroundPanel.add(dealerCardsPanel);

        //bottom panel
        JPanel bottomPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 40, 0));
        bottomPanel.setOpaque(false);
        bottomPanel.setBounds(55, 720, 1480, 120);
        backgroundPanel.add(bottomPanel);

        //stack panel
        stackPanel = new StackPanel(controller);
        bottomPanel.add(stackPanel);

        //bet panel
        betPanel = new BetPanel(controller);
        bottomPanel.add(betPanel);

        //options panel
        optionsPanel = new OptionsPanel(controller);
        backgroundPanel.add(optionsPanel);

        //message panel
        messagePanel = new MessagePanel();
        backgroundPanel.add(messagePanel);
        mainFrame.setVisible(true);
    }

    @Override
    public void updateStack() {
        Player p = (Player) player;
        stackPanel.setStack(p.getStack());
        stackPanel.refreshStack();
    }

    @Override
    public void updateBet() {
        betPanel.setBet(round.getBet());
        betPanel.refreshBet();
    }

    @Override
    public void enableOptionsPanel() {
        optionsPanel.setVisible(true);
    }

    @Override
    public void enableBetPanel() {
        betPanel.setVisible(true);
    }

    @Override
    public void enableCardsPanels() {
        playerCardsPanel.setVisible(true);
        dealerCardsPanel.setVisible(true);
    }

    @Override
    public void enableMessagePanel() {
        messagePanel.setVisible(true);
    }

    @Override
    public void disableMessagePanel() {
        messagePanel.setVisible(false);
    }

    @Override
    public void enableNextRoundButton() {
        optionsPanel.getNextRoundButton().setEnabled(true);
    }

    @Override
    public void disableNextRoundButton() {
        optionsPanel.getNextRoundButton().setEnabled(false);
    }

    @Override
    public void enableDealButton() {
        optionsPanel.getDealButton().setEnabled(true);
    }

    @Override
    public void disableDealButton() {
        optionsPanel.getDealButton().setEnabled(false);
    }

    @Override
    public void enableHitButton() {
        optionsPanel.getHitButton().setEnabled(true);
    }

    @Override
    public void enableStandButton() {
        optionsPanel.getStandButton().setEnabled(true);
    }

    @Override
    public void enableDoubleButton() {
        optionsPanel.getDoubleButton().setEnabled(true);
    }

    @Override
    public void disableDoubleButton() {
        optionsPanel.getDoubleButton().setEnabled(false);
    }

    @Override
    public void disableStartButton() {
        stackPanel.getStartButton().setEnabled(false);
    }

    @Override
    public void enableExitButton() {
        stackPanel.getExitButton().setEnabled(true);
    }

    @Override
    public void disableExitButton() {
        stackPanel.getExitButton().setEnabled(false);
    }

    @Override
    public void disableAllChips() {
        betPanel.getChip_1().setEnabled(false);
        betPanel.getChip_5().setEnabled(false);
        betPanel.getChip_25().setEnabled(false);
        betPanel.getChip_100().setEnabled(false);
    }

    @Override
    public void disableOptionButtons() {
        optionsPanel.disableButtons();
    }

    @Override
    public void showPlayerFirstTwoCards() {
        playerCardsPanel.showPlayerFirstTwoCards();
        playerValueLabel.setText(String.valueOf(player.getCardsValue()));
    }

    @Override
    public void showPlayerNextCard() {
        playerCardsPanel.showPlayerNextCard();
        playerValueLabel.setText(String.valueOf(player.getCardsValue()));
    }

    @Override
    public void showDealerTwoCardsSecondHidden() {
        dealerCardsPanel.showDealerSecondCardHidden();
        dealerValueLabel.setText(String.valueOf(dealer.getListOfCards().get(0).getValue()));
    }

    @Override
    public void showAllDealerCards() {
        dealerCardsPanel.showAllCards();
        dealerValueLabel.setText(String.valueOf(dealer.getCardsValue()));
    }

    @Override
    public void clearCards() {
        playerCardsPanel.clearCards();
        dealerCardsPanel.clearCards();
        playerValueLabel.setText(String.valueOf(player.getCardsValue()));
        dealerValueLabel.setText(String.valueOf(dealer.getCardsValue()));
    }

    @Override
    public void setMessage(String message) {
        disableMessagePanel();
        enableMessagePanel();
        messagePanel.setMessage(message);
    }

    @Override
    public String getInitialStack() {
        return stackPanel.getInitialStackTextField().getText().replaceAll(",", "");
    }

    @Override
    public void disableInitialStackTExtField() {
        stackPanel.getInitialStackTextField().setEnabled(false);
    }
}

