package com.michal.blackjack.view;

public interface Observer {
    void updateStack();

    void updateBet();
}
