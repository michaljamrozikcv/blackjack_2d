package com.michal.blackjack.view.components;

import javax.swing.*;
import java.awt.*;

class AmountLabel extends JLabel {
    private final int fontSize = 50;
    private final Color foregroundColor = Color.ORANGE;

    public AmountLabel(String text, int horizontalAlignment) {
        super(text, horizontalAlignment);
        setForeground(foregroundColor);
        setFont(new Font("Bernard MT Condensed", Font.BOLD, fontSize));
    }
}
