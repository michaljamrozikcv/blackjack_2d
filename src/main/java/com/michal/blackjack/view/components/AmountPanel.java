package com.michal.blackjack.view.components;

import javax.swing.*;
import java.awt.*;

public class AmountPanel extends JPanel {
    private final Dimension dimension = new Dimension(350, 120);
    private final int cols = 1;
    private final int rows = 2;
    private final int hgap = 80;
    private final int vgap = 0;

    AmountPanel() {
        setLayout(new GridLayout(rows, cols, hgap, vgap));
        setPreferredSize(dimension);
        setOpaque(false);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
    }
}
