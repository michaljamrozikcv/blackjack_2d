package com.michal.blackjack.view.components;

import com.michal.blackjack.controller.BlackjackController;
import lombok.Getter;
import lombok.Setter;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

@Getter
@Setter
public class BetPanel extends JPanel implements ActionListener {
    private final ChipButton chip_1;
    private final ChipButton chip_5;
    private final ChipButton chip_25;
    private final ChipButton chip_100;
    private final JPanel betAmountPanel;
    private final JLabel betLabel;
    private BlackjackController controller;
    private int bet;

    public BetPanel(BlackjackController controller) {
        this.controller = controller;
        betAmountPanel = new AmountPanel();
        betLabel = new AmountLabel(Formatter.displayAmount(Double.valueOf(bet)), SwingConstants.LEFT);
        betAmountPanel.add(new HeaderLabel("Bet", SwingConstants.LEFT));
        betAmountPanel.add(betLabel);
        add(betAmountPanel);
        chip_1 = new ChipButton("chips/chip_1.png", 1);
        chip_5 = new ChipButton("chips/chip_5.png", 5);
        chip_25 = new ChipButton("chips/chip_25.png", 25);
        chip_100 = new ChipButton("chips/chip_100.png", 100);
        chip_1.addActionListener(this);
        chip_5.addActionListener(this);
        chip_25.addActionListener(this);
        chip_100.addActionListener(this);
        add(chip_1);
        add(chip_5);
        add(chip_25);
        add(chip_100);
        setOpaque(false);
        setVisible(false);
    }

    public List<ChipButton> getListOfChips() {
        return List.of(chip_1, chip_5, chip_25, chip_100);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        ChipButton selected = (ChipButton) e.getSource();
        controller.updateBet(selected.getValue());
    }

    public void refreshBet() {
        betLabel.setText(Formatter.displayAmount(Double.valueOf(bet)));
    }
}


