package com.michal.blackjack.view.components;

import javax.swing.*;
import java.awt.*;

public class CardPanel extends JPanel {
    private final Image image;
    private final int width = 150;
    private final int height = 230;

    CardPanel(String fileName) {
        this.image = new ImageIcon(getClass().getResource(fileName)).getImage()
                .getScaledInstance(width, height, Image.SCALE_SMOOTH);
        setLayout(null);
    }

    @Override
    protected void paintComponent(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;
        g2d.drawImage(image, 0, 0, this);
    }
}
