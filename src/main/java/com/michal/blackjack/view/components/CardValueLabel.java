package com.michal.blackjack.view.components;

import javax.swing.*;
import java.awt.*;

public class CardValueLabel extends JLabel {
    private final int fontSize = 45;
    private final int x = 10;
    private final int y = 0;
    private final int width = 50;
    private final int height = 50;
    private final Color foregroundColor = Color.ORANGE;

    public CardValueLabel(String text) {
        super(text);
        setForeground(foregroundColor);
        setFont(new Font("Bernard MT Condensed", Font.BOLD, fontSize));
        setBounds(x, y, width, height);
    }
}
