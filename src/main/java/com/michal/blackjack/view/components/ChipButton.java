package com.michal.blackjack.view.components;

import lombok.Getter;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Ellipse2D;

@Getter
public class ChipButton extends JButton {
    private final int height = 110;
    private final int width = 110;
    private final int value;
    private Shape shape;

    ChipButton(String fileName, int value) {
        ImageIcon imageIcon = new ImageIcon(getClass().getClassLoader().getResource(fileName));
        Icon icon = new ImageIcon(imageIcon.getImage().getScaledInstance(width, height, Image.SCALE_SMOOTH));
        setIcon(icon);
        String fileNameHover = fileName.substring(0, fileName.indexOf(".")) + "_hover.png";
        ImageIcon imageIconHover = new ImageIcon(getClass().getClassLoader().getResource(fileNameHover));
        Icon iconHover = new ImageIcon(imageIconHover.getImage().getScaledInstance(width, height, Image.SCALE_SMOOTH));
        setRolloverIcon(iconHover);
        setPreferredSize(new Dimension(width, height));
        setContentAreaFilled(false);
        this.value = value;
        setBorder(BorderFactory.createEmptyBorder());
    }

    @Override
    public boolean contains(int x, int y) {
        if (shape == null || !shape.getBounds().equals(getBounds())) {
            shape = new Ellipse2D.Float(0, 0, getWidth(), getHeight());
        }
        return shape.contains(x, y);
    }
}
