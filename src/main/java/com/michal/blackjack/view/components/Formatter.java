package com.michal.blackjack.view.components;

import java.util.Locale;

public class Formatter {
    public static String displayAmount(Double number) {
        StringBuilder sb = new StringBuilder("$ ");
        sb.append(String.format(Locale.US, "%1$,.2f", number));
        return sb.toString();
    }
}
