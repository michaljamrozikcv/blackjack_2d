package com.michal.blackjack.view.components;

import javax.swing.*;
import java.awt.*;

public class GamerLabel extends JLabel {
    private final int fontSize = 50;
    private final int x = 0;
    private final int y = 0;
    private final int width = 230;
    private final int height = 230;
    private final Color foregroundColor = Color.WHITE;

    GamerLabel(String text) {
        super(text);
        setForeground(foregroundColor);
        setFont(new Font("Bernard MT Condensed", Font.BOLD, fontSize));
        setBounds(x, y, width, height);
        setVerticalAlignment(SwingConstants.NORTH);
        setHorizontalAlignment(SwingConstants.LEFT);
    }

    @Override
    protected void paintComponent(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;
        g2d.rotate(Math.toRadians(-90), getX() + getWidth() / 2, getY() + getHeight() / 2);
        super.paintComponent(g);
    }
}
