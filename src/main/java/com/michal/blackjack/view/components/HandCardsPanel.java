package com.michal.blackjack.view.components;

import com.michal.blackjack.model.PlayingCard;

import javax.swing.*;
import java.awt.*;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class HandCardsPanel extends JPanel {
    private final int panelWidth = 800;
    private final int panelHeight = 230;
    private final int cardWidth = 150;
    private final int cardHeight = 230;
    private final int cardsPanelWidth = 750;
    private final int cardsPanelHeight = 230;
    private final int cardsPanelLeftBorder = 50;
    private final int cardsPanelUpperBorder = 0;
    private final AtomicInteger layer = new AtomicInteger();
    private final int delta = 50;
    private final String labelName;
    private final List<PlayingCard> listOfCards;
    private final JLayeredPane cardsPanel;
    private AtomicInteger reallocation = new AtomicInteger(0);

    public HandCardsPanel(String labelName, int x, int y, List<PlayingCard> listOfCards) {
        this.labelName = labelName;
        this.listOfCards = listOfCards;
        setBounds(x, y, panelWidth, panelHeight);
        setLayout(null);
        setOpaque(false);
        JLabel gamerLabel = new GamerLabel(labelName);
        add(gamerLabel);
        cardsPanel = new JLayeredPane();
        cardsPanel.setLayout(null);
        cardsPanel.setOpaque(false);
        cardsPanel.setBounds(cardsPanelLeftBorder, cardsPanelUpperBorder, cardsPanelWidth, cardsPanelHeight);
        add(cardsPanel);
        setVisible(false);
    }

    public void showPlayerFirstTwoCards() {
        createNewCardPanel(listOfCards.get(0).toString());
        createNewCardPanel(listOfCards.get(1).toString());
    }

    public void showPlayerNextCard() {
        createNewCardPanel(listOfCards.get(listOfCards.size() - 1).toString());
    }

    public void showDealerSecondCardHidden() {
        createNewCardPanel(listOfCards.get(0).toString());
        createNewCardPanel("back");
    }

    public void showAllCards() {
        clearCards();
        if (listOfCards != null) {
            listOfCards.forEach(playingCard -> createNewCardPanel(playingCard.toString()));
        }
    }

    public void clearCards() {
        cardsPanel.removeAll();
        reallocation.set(0);
        repaint();
    }

    private void createNewCardPanel(String fileName) {
        JPanel panel = new CardPanel("/cards/" + fileName + ".png");
        panel.setBounds(reallocation.addAndGet(delta), 0, cardWidth, cardHeight);
        cardsPanel.add(panel, Integer.valueOf(layer.addAndGet(1)));
        panel.setOpaque(false);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
    }
}
