package com.michal.blackjack.view.components;

import javax.swing.*;
import java.awt.*;

public class HeaderLabel extends JLabel {
    private final int fontSize = 30;
    private final Color foregroundColor = Color.WHITE;

    HeaderLabel(String text, int horizontalAlignment) {
        super(text, horizontalAlignment);
        setForeground(foregroundColor);
        setFont(new Font("Bernard MT Condensed", Font.BOLD, fontSize));
    }
}
