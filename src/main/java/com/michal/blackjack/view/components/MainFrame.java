package com.michal.blackjack.view.components;

import javax.swing.*;
import java.awt.*;

public class MainFrame extends JFrame {
    private final int width = 1600;
    private final int height = 900;

    public MainFrame() throws HeadlessException {
        setTitle("Blackjack");
        setMinimumSize(new Dimension(width, height));
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        setLayout(null);
    }
}
