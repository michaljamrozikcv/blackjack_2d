package com.michal.blackjack.view.components;

import javax.swing.*;
import java.awt.*;

class MessageLabel extends JLabel {
    private final int fontSize = 33;
    private final Color fontColor = Color.ORANGE;

    MessageLabel() {
        setForeground(fontColor);
        setFont(new Font("Bernard MT Condensed", Font.PLAIN, fontSize));
    }
}
