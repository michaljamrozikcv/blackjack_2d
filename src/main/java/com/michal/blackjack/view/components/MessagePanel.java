package com.michal.blackjack.view.components;

import javax.swing.*;
import java.awt.*;

public class MessagePanel extends JPanel {
    private final Color backgroundColor = Color.ORANGE;
    private final int width = 1590;
    private final int height = 70;
    private final int leftBorder = 0;
    private final int upperBorder = 320;
    private final JLabel messageLabel;

    public MessagePanel() {
        setBounds(leftBorder, upperBorder, width, height);
        setBackground(CustomColors.black);
        messageLabel = new MessageLabel();
        setLayout(new GridBagLayout());
        add(messageLabel);
        setMessage("Welcome!  Please provide initial stack (max $ 9,999,999)");
        setBorder(BorderFactory.createMatteBorder(2, 0, 2, 0, backgroundColor));
    }

    public void setMessage(String message) {
        messageLabel.setText(message);
    }
}


