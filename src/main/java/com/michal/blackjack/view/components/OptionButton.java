package com.michal.blackjack.view.components;

import javax.swing.*;
import java.awt.*;

class OptionButton extends JButton {
    private final int width = 200;
    private final int height = 50;
    private final int fontSize = 30;
    private final Color foregroundColor = Color.WHITE;

    OptionButton(String text) {
        super(text);
        setPreferredSize(new Dimension(width, height));
        setBackground(CustomColors.blue);
        setForeground(foregroundColor);
        setFocusPainted(false);
        setFont(new Font("Bernard MT Condensed", Font.PLAIN, fontSize));
    }
}
