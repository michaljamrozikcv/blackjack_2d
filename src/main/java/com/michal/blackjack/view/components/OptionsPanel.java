package com.michal.blackjack.view.components;

import com.michal.blackjack.controller.BlackjackController;
import lombok.Getter;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

@Getter
public class OptionsPanel extends JPanel implements ActionListener {
    private final JButton nextRoundButton;
    private final JButton dealButton;
    private final JButton hitButton;
    private final JButton standButton;
    private final JButton doubleButton;
    private final int width = 200;
    private final int height = 280;
    private final int leftBorder = 100;
    private final int upperBorder = 450;
    private BlackjackController controller;

    public OptionsPanel(BlackjackController controller) {
        this.controller = controller;
        setOpaque(false);
        setBounds(leftBorder, upperBorder, width, height);
        nextRoundButton = new OptionButton("Next Round");
        dealButton = new OptionButton("Deal");
        nextRoundButton.setBackground(CustomColors.green);
        dealButton.setBackground(CustomColors.green);
        hitButton = new OptionButton("Hit");
        standButton = new OptionButton("Stand");
        doubleButton = new OptionButton("Double");
        nextRoundButton.addActionListener(this);
        dealButton.addActionListener(this);
        hitButton.addActionListener(this);
        standButton.addActionListener(this);
        doubleButton.addActionListener(this);
        add(nextRoundButton);
        add(dealButton);
        add(hitButton);
        add(standButton);
        add(doubleButton);
        disableButtons();
        nextRoundButton.setEnabled(false);
        setVisible(false);
    }

    public void disableButtons() {
        dealButton.setEnabled(false);
        hitButton.setEnabled(false);
        standButton.setEnabled(false);
        doubleButton.setEnabled(false);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == nextRoundButton) {
            controller.nextRound();
        } else if (e.getSource() == dealButton) {
            controller.deal();
        } else if (e.getSource() == hitButton) {
            controller.hit();
        } else if (e.getSource() == standButton) {
            controller.stand();
        } else if (e.getSource() == doubleButton) {
            controller.doubleBet();
        }
    }
}