package com.michal.blackjack.view.components;

import com.michal.blackjack.controller.BlackjackController;
import lombok.Getter;
import lombok.Setter;

import javax.swing.*;
import javax.swing.text.NumberFormatter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.NumberFormat;
import java.util.Locale;

@Getter
@Setter
public class StackPanel extends JPanel implements ActionListener {
    private final JPanel currentStackPanel;
    private final JLabel currentStackLabel;
    private final JButton startButton;
    private final JButton exitButton;
    private final JFormattedTextField initialStackTextField;
    private final Font font = new Font("Bernard MT Condensed", Font.PLAIN, 23);
    private BlackjackController controller;
    private double stack;
    private final Color foregroundColor = Color.WHITE;

    public StackPanel(BlackjackController controller) {
        this.controller = controller;
        JPanel initialStackPanel = new JPanel(new GridLayout(2, 2, 5, 5));
        initialStackPanel.setPreferredSize(new Dimension(200, 110));
        JLabel initialStackLabel = new JLabel("Buy-in ($)", SwingConstants.CENTER);
        initialStackPanel.add(initialStackLabel);
        initialStackLabel.setForeground(foregroundColor);
        initialStackLabel.setFont(font);

        NumberFormat format = NumberFormat.getNumberInstance(Locale.US);
        format.setMaximumFractionDigits(0);
        format.setMaximumIntegerDigits(7);
        NumberFormatter formatter = new NumberFormatter(format);
        initialStackTextField = new JFormattedTextField(formatter);

        initialStackTextField.setFont(font);
        startButton = new OptionButton("Start");
        startButton.setBackground(CustomColors.green);
        exitButton = new OptionButton("Exit");
        exitButton.setBackground(CustomColors.green);
        exitButton.setEnabled(false);
        startButton.addActionListener(this);
        exitButton.addActionListener(this);
        initialStackPanel.add(initialStackTextField);
        initialStackPanel.add(startButton);
        initialStackPanel.add(exitButton);
        initialStackPanel.setOpaque(false);
        add(initialStackPanel);
        currentStackPanel = new AmountPanel();
        currentStackLabel = new AmountLabel(Formatter.displayAmount(stack), SwingConstants.RIGHT);
        currentStackPanel.add(new HeaderLabel("Stack", SwingConstants.RIGHT));
        currentStackPanel.add(currentStackLabel);
        add(currentStackPanel);
        setOpaque(false);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == startButton) {
            controller.startGame();
            refreshStack();
        } else if (e.getSource() == exitButton) {
            controller.exit();
        }
    }

    public void refreshStack() {
        currentStackLabel.setText(Formatter.displayAmount(stack));
    }
}

