package com.michal.blackjack.controller;

import com.michal.blackjack.model.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.mockito.Mockito.when;

class BlackjackControllerImplTest {
    private Player player = new Player();
    private Gambler dealer = new Gamer();
    private Round round = new Round();
    private DealingShoe dealingShoeMock = Mockito.mock(DealingShoe.class);
    private BlackjackController tested = new BlackjackControllerImpl(player, dealer, round, dealingShoeMock);

    @BeforeEach
    public void setup() {
        tested.createGame();
        tested.startGame();
        player.updateStack(10);
        tested.updateBet(10);
    }

    @Test
    @DisplayName("dealer has blackjack and player has lower value after deal")
    public void blackjack_dealer_win() {
        when(dealingShoeMock.takeCard()).thenReturn(new PlayingCard(Rank.TWO, Suit.CLUB))
                .thenReturn(new PlayingCard(Rank.ACE, Suit.CLUB))
                .thenReturn(new PlayingCard(Rank.TWO, Suit.CLUB))
                .thenReturn(new PlayingCard(Rank.JACK, Suit.CLUB));
        tested.deal();
        Assertions.assertEquals(0, player.getStack());
    }

    @Test
    @DisplayName("player has blackjack and dealer has lower value after deal")
    public void blackjack_player_win() {
        when(dealingShoeMock.takeCard()).thenReturn(new PlayingCard(Rank.ACE, Suit.CLUB))
                .thenReturn(new PlayingCard(Rank.THREE, Suit.CLUB))
                .thenReturn(new PlayingCard(Rank.KING, Suit.CLUB))
                .thenReturn(new PlayingCard(Rank.FIVE, Suit.CLUB));
        tested.deal();
        Assertions.assertEquals(25, player.getStack());
    }

    @Test
    @DisplayName("both player and dealer have blackjack")
    public void blackjack_draw() {
        when(dealingShoeMock.takeCard()).thenReturn(new PlayingCard(Rank.ACE, Suit.CLUB))
                .thenReturn(new PlayingCard(Rank.ACE, Suit.CLUB))
                .thenReturn(new PlayingCard(Rank.KING, Suit.CLUB))
                .thenReturn(new PlayingCard(Rank.QUEEN, Suit.CLUB));
        tested.deal();
        Assertions.assertEquals(10, player.getStack());
    }

    @Test
    @DisplayName("no blackjack after deal player hit one card and lose value higher than 21")
    public void player_hit_and_lose() {
        when(dealingShoeMock.takeCard()).thenReturn(new PlayingCard(Rank.TWO, Suit.CLUB))
                .thenReturn(new PlayingCard(Rank.KING, Suit.CLUB))
                .thenReturn(new PlayingCard(Rank.KING, Suit.CLUB))
                .thenReturn(new PlayingCard(Rank.QUEEN, Suit.CLUB))
                .thenReturn(new PlayingCard(Rank.QUEEN, Suit.CLUB));
//        round.updateBet(10);
        tested.deal();
        tested.hit();
        Assertions.assertEquals(0, player.getStack());
    }

    @Test
    @DisplayName("no blackjack after deal player hit one card has 21 dealer has 19")
    public void player_hit_has_21_and_win() {
        //when player has 21, there is no need to stand - this step is processed automatically
        //dealer has 19 - he doesn't hit card
        when(dealingShoeMock.takeCard()).thenReturn(new PlayingCard(Rank.TWO, Suit.CLUB))
                .thenReturn(new PlayingCard(Rank.NINE, Suit.CLUB))
                .thenReturn(new PlayingCard(Rank.KING, Suit.CLUB))
                .thenReturn(new PlayingCard(Rank.QUEEN, Suit.CLUB))
                .thenReturn(new PlayingCard(Rank.NINE, Suit.CLUB));
        tested.deal();
        tested.hit();
        Assertions.assertEquals(20, player.getStack());
    }

    @Test
    @DisplayName("no_blackjack_after_deal_player_hit_one_card_has_21_and_dealer_has_15_and_hit_and_has_18(")
    public void player_hit_has_21_and_win_2() {
        //when player has 21, there is no need to stand - this step is processed automatically
        //dealer has 15 - he hits and lose because he has 18 and cannot hit more cards
        when(dealingShoeMock.takeCard()).thenReturn(new PlayingCard(Rank.TWO, Suit.CLUB))
                .thenReturn(new PlayingCard(Rank.NINE, Suit.CLUB))
                .thenReturn(new PlayingCard(Rank.KING, Suit.CLUB))
                .thenReturn(new PlayingCard(Rank.TWO, Suit.CLUB))
                .thenReturn(new PlayingCard(Rank.NINE, Suit.CLUB))
                .thenReturn(new PlayingCard(Rank.TWO, Suit.CLUB))
                .thenReturn(new PlayingCard(Rank.FIVE, Suit.CLUB));
        tested.deal();
        tested.hit();
        Assertions.assertEquals(20, player.getStack());
    }

    @Test
    @DisplayName("no_blackjack_after_deal_player_hit_one_card_has_21_and_dealer_has_15_and_hit_and_lose_and_has_22")
    public void player_hit_has_21_and_win_3() {
        //when player has 21, there is no need to stand - this step is processed automatically
        //dealer has 15 - he hits and lose because he has more than 21
        when(dealingShoeMock.takeCard()).thenReturn(new PlayingCard(Rank.TWO, Suit.CLUB))
                .thenReturn(new PlayingCard(Rank.NINE, Suit.CLUB))
                .thenReturn(new PlayingCard(Rank.KING, Suit.CLUB))
                .thenReturn(new PlayingCard(Rank.TWO, Suit.CLUB))
                .thenReturn(new PlayingCard(Rank.NINE, Suit.CLUB))
                .thenReturn(new PlayingCard(Rank.TWO, Suit.CLUB))
                .thenReturn(new PlayingCard(Rank.KING, Suit.CLUB));
        tested.deal();
        tested.hit();
        Assertions.assertEquals(20, player.getStack());
    }

    @Test
    @DisplayName("no_blackjack_after_deal_player_hit_one_card_has_21_and_dealer_has_15_and_hit_and_has_21()")
    public void player_hit_has_21_and_draw() {
        //when player has 21, there is no need to stand - this step is processed automatically
        //dealer has 15 - he hits and we have draw both gamers have 21
        when(dealingShoeMock.takeCard()).thenReturn(new PlayingCard(Rank.TWO, Suit.CLUB))
                .thenReturn(new PlayingCard(Rank.NINE, Suit.CLUB))
                .thenReturn(new PlayingCard(Rank.KING, Suit.CLUB))
                .thenReturn(new PlayingCard(Rank.TWO, Suit.CLUB))
                .thenReturn(new PlayingCard(Rank.NINE, Suit.CLUB))
                .thenReturn(new PlayingCard(Rank.TWO, Suit.CLUB))
                .thenReturn(new PlayingCard(Rank.EIGHT, Suit.CLUB));
        tested.deal();
        tested.hit();
        Assertions.assertEquals(10, player.getStack());
    }

    @Test
    @DisplayName("no_blackjack_after_deal_player_has_18_and_stand_dealer_has_22()")
    public void player_stand_and_win() {
        //dealer has 18 - he hits and lose because he has more than 21
        when(dealingShoeMock.takeCard()).thenReturn(new PlayingCard(Rank.NINE, Suit.CLUB))
                .thenReturn(new PlayingCard(Rank.TEN, Suit.CLUB))
                .thenReturn(new PlayingCard(Rank.NINE, Suit.CLUB))
                .thenReturn(new PlayingCard(Rank.FIVE, Suit.CLUB))
                .thenReturn(new PlayingCard(Rank.SEVEN, Suit.CLUB));
        tested.deal();
        tested.stand();
        Assertions.assertEquals(20, player.getStack());
    }

    @Test
    @DisplayName("no_blackjack_after_deal_player_has_18_and_stand_dealer_has_20()")
    public void player_stand_and_lose() {
        when(dealingShoeMock.takeCard()).thenReturn(new PlayingCard(Rank.KING, Suit.CLUB))
                .thenReturn(new PlayingCard(Rank.NINE, Suit.CLUB))
                .thenReturn(new PlayingCard(Rank.EIGHT, Suit.CLUB))
                .thenReturn(new PlayingCard(Rank.TWO, Suit.CLUB))
                .thenReturn(new PlayingCard(Rank.TWO, Suit.CLUB))
                .thenReturn(new PlayingCard(Rank.TWO, Suit.CLUB))
                .thenReturn(new PlayingCard(Rank.FIVE, Suit.CLUB));
        tested.deal();
        tested.stand();
        Assertions.assertEquals(0, player.getStack());
    }

    @Test
    @DisplayName("no_blackjack_after_deal_player_has_18_and_stand_dealer_has_18()")
    public void player_stand_and_draw() {
        when(dealingShoeMock.takeCard()).thenReturn(new PlayingCard(Rank.KING, Suit.CLUB))
                .thenReturn(new PlayingCard(Rank.NINE, Suit.CLUB))
                .thenReturn(new PlayingCard(Rank.EIGHT, Suit.CLUB))
                .thenReturn(new PlayingCard(Rank.TWO, Suit.CLUB))
                .thenReturn(new PlayingCard(Rank.TWO, Suit.CLUB))
                .thenReturn(new PlayingCard(Rank.TWO, Suit.CLUB))
                .thenReturn(new PlayingCard(Rank.THREE, Suit.CLUB));
        tested.deal();
        tested.stand();
        Assertions.assertEquals(10, player.getStack());
    }

    @Test
    @DisplayName("dealer_hit_soft_17_and_win_with_value_18_and_2_ACES")
    public void player_stand_and_lose_dealer_with_soft_17_hit() {
        when(dealingShoeMock.takeCard()).thenReturn(new PlayingCard(Rank.ACE, Suit.CLUB))
                .thenReturn(new PlayingCard(Rank.ACE, Suit.CLUB))
                .thenReturn(new PlayingCard(Rank.FIVE, Suit.CLUB))
                .thenReturn(new PlayingCard(Rank.SIX, Suit.CLUB))
                .thenReturn(new PlayingCard(Rank.ACE, Suit.CLUB));
        tested.deal();
        tested.stand();
        Assertions.assertEquals(0, player.getStack());
    }

    @Test
    public void player_hit_just_ACES() {
        when(dealingShoeMock.takeCard()).thenReturn(new PlayingCard(Rank.ACE, Suit.CLUB))
                .thenReturn(new PlayingCard(Rank.ACE, Suit.CLUB))
                .thenReturn(new PlayingCard(Rank.ACE, Suit.CLUB))
                .thenReturn(new PlayingCard(Rank.SEVEN, Suit.CLUB))
                .thenReturn(new PlayingCard(Rank.ACE, Suit.CLUB))
                .thenReturn(new PlayingCard(Rank.ACE, Suit.CLUB));
        tested.deal();
        tested.hit();
        Assertions.assertEquals(13, player.getCardsValue());
    }

    @Test
    @DisplayName("player double and win dealer has more than 21")
    public void player_double_and_win() {
        when(dealingShoeMock.takeCard()).thenReturn(new PlayingCard(Rank.ACE, Suit.CLUB))
                .thenReturn(new PlayingCard(Rank.TEN, Suit.CLUB))
                .thenReturn(new PlayingCard(Rank.FIVE, Suit.CLUB))
                .thenReturn(new PlayingCard(Rank.SIX, Suit.CLUB))
                .thenReturn(new PlayingCard(Rank.SIX, Suit.CLUB))
                .thenReturn(new PlayingCard(Rank.TEN, Suit.CLUB));
        tested.deal();
        player.updateStack(10);
        tested.doubleBet();
        Assertions.assertEquals(40, player.getStack());
    }

    @Test
    @DisplayName("player double and win dealer has less value")
    public void player_double_and_win_2() {
        when(dealingShoeMock.takeCard()).thenReturn(new PlayingCard(Rank.ACE, Suit.CLUB))
                .thenReturn(new PlayingCard(Rank.TEN, Suit.CLUB))
                .thenReturn(new PlayingCard(Rank.FOUR, Suit.CLUB))
                .thenReturn(new PlayingCard(Rank.SEVEN, Suit.CLUB))
                .thenReturn(new PlayingCard(Rank.SIX, Suit.CLUB));
        tested.deal();
        player.updateStack(10);
        tested.doubleBet();
        Assertions.assertEquals(40, player.getStack());
    }

    @Test
    @DisplayName("player double and has value higher than 21 and lose")
    public void player_double_and_lose() {
        when(dealingShoeMock.takeCard()).thenReturn(new PlayingCard(Rank.TEN, Suit.CLUB))
                .thenReturn(new PlayingCard(Rank.TEN, Suit.CLUB))
                .thenReturn(new PlayingCard(Rank.TEN, Suit.CLUB))
                .thenReturn(new PlayingCard(Rank.SEVEN, Suit.CLUB))
                .thenReturn(new PlayingCard(Rank.TEN, Suit.CLUB));
        player.updateStack(10);
        tested.deal();
        tested.doubleBet();
        Assertions.assertEquals(0, player.getStack());
    }

    @Test
    @DisplayName("player double and lose dealer has greater value")
    public void player_double_and_lose_2() {
        when(dealingShoeMock.takeCard()).thenReturn(new PlayingCard(Rank.TEN, Suit.CLUB))
                .thenReturn(new PlayingCard(Rank.ACE, Suit.CLUB))
                .thenReturn(new PlayingCard(Rank.SEVEN, Suit.CLUB))
                .thenReturn(new PlayingCard(Rank.NINE, Suit.CLUB))
                .thenReturn(new PlayingCard(Rank.TWO, Suit.CLUB));
        player.updateStack(10);
        tested.deal();
        tested.doubleBet();
        Assertions.assertEquals(0, player.getStack());
    }

    @Test
    @DisplayName("player double and draw")
    public void player_double_and_draw() {
        when(dealingShoeMock.takeCard()).thenReturn(new PlayingCard(Rank.TEN, Suit.CLUB))
                .thenReturn(new PlayingCard(Rank.ACE, Suit.CLUB))
                .thenReturn(new PlayingCard(Rank.SEVEN, Suit.CLUB))
                .thenReturn(new PlayingCard(Rank.NINE, Suit.CLUB))
                .thenReturn(new PlayingCard(Rank.THREE, Suit.CLUB));
        player.updateStack(10);
        tested.deal();
        tested.doubleBet();
        Assertions.assertEquals(20, player.getStack());
    }
}

