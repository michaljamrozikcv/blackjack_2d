package com.michal.blackjack.model;

import org.junit.jupiter.api.Test;

import java.util.Arrays;;
import static org.junit.jupiter.api.Assertions.*;

class DealingShoeImplTest {
    private DealingShoeImpl tested = new DealingShoeImpl();

    @Test
    public void should_remove_one_card_from_dealing_shoe() {
        tested.takeCard();
        assertEquals(tested.getQuantityOfDecks() * 52 - 1, tested.getListOfCards().size());
    }

    @Test
    public void should_shuffle_cards() {
        DealingShoeImpl dealingShoeImplNotShuffled = new DealingShoeImpl();
        tested.shuffle();
        assertFalse(Arrays.equals(dealingShoeImplNotShuffled.getListOfCards().toArray(), tested.getListOfCards().toArray()));

    }
}